const _ = require('lodash')
const Promise = require('bluebird')
const path = require('path')
const webpackLodashPlugin = require('lodash-webpack-plugin')

const seriesMap = require('./src/pages/series.json')

const NOT_IN_SERIES = '***NOT_IN_SERIES***'

const seriesSlugList = Object.keys(seriesMap)
const postSlugToSeries = seriesSlugList.reduce((acc, seriesSlug) => {
  const posts = seriesMap[seriesSlug].posts
  return !posts
    ? acc
    : {
      ...acc,
      ...posts.reduce((iacc, postSlug) => ({ ...iacc, [postSlug]: seriesSlug }), {}),
    }
}, {})

// Helpers

function slugToPath (slug) {
  return `/posts/${slug}`
}

function tagToPath (tag) {
  return `/tags/${_.kebabCase(tag)}`
}

function categoryToPath (category) {
  return `/category/${_.kebabCase(category)}`
}

function seriesToPath (series) {
  return `/series/${_.kebabCase(series)}`
}

// Gatsby

/**
 * 1. Grab our page templates
 * 2. Get all blog posts through graphql
 * 3. Create a page for each blog post
 * 4. Reduce list of all tags from posts, and create a page for each
 * 5. Reduce list of all categories from posts, and create a page for each
 * 6. Reduce list of all series from posts, and create a page for each
 */
exports.createPages = ({ graphql, boundActionCreators }) => {
  const { createPage } = boundActionCreators

  return new Promise((resolve) => {
    const postPageTemplate = path.resolve('src/templates/blog-post.js')
    const tagPageTemplate = path.resolve('src/templates/tag-page.js')
    const categoryPageTemplate = path.resolve('src/templates/category-page.js')
    const seriesPageTemplate = path.resolve('src/templates/series-page.js')

    graphql(`
      {
        allMarkdownRemark(limit: 2000, filter: { frontmatter: { draft: { ne: true } } }) {
          edges {
            node {
              fields {
                slugWithPath {
                  slug
                  path
                }
                tagsWithPaths {
                  tag
                  path
                }
              }
              frontmatter {
                category
              }
            }
          }
        }
      }
    `).then((result) => {
      if (result.errors) {
        console.log(result.errors)
        resolve()
        // reject(result.errors)
      }

      // Create blog post pages

      _.each(result.data.allMarkdownRemark.edges, (edge) => {
        const slugWithPath = edge.node.fields.slugWithPath

        createPage({
          path: slugWithPath.path,
          component: postPageTemplate,
          context: {
            slug: slugWithPath.slug,
          },
        })
      })

      // Create tag pages

      // Collect all tags (with their paths -- see onCreateNode)
      const allTagsWithPaths = result.data.allMarkdownRemark.edges.reduce((acc, edge) => {
        const postTags = _.get(edge, 'node.fields.tagsWithPaths')
        return postTags ? [...acc, ...postTags] : acc
      }, [])
      // Unique by the 'tag' key
      const uniqueTagsWithPaths = _.uniqBy(allTagsWithPaths, 'tag')
      // Create pages
      uniqueTagsWithPaths.forEach((tagWithPath) => {
        createPage({
          path: tagWithPath.path,
          component: tagPageTemplate,
          context: {
            tag: tagWithPath.tag,
          },
        })
      })

      // Create category pages

      const categories = _.uniq(
        result.data.allMarkdownRemark.edges.reduce((acc, edge) => {
          const postCategory = _.get(edge, 'node.frontmatter.category')
          return postCategory ? [...acc, postCategory] : acc
        }, []),
      )

      categories.forEach((category) => {
        createPage({
          path: categoryToPath(category),
          component: categoryPageTemplate,
          context: {
            category,
          },
        })
      })

      // Create post series pages

      seriesSlugList.forEach((slug) => {
        const series = seriesMap[slug]

        createPage({
          path: seriesToPath(slug),
          component: seriesPageTemplate,
          context: {
            seriesSlug: slug,
            series: {
              slug,
              ...series,
            },
          },
        })
      })

      resolve()
    })
  })
}

/**
 * 1. Handle File node creation from gatsby-plugin-filesystem,
 *    adding slug fields to appropriately well-formed md files
 * 2. Handle MarkdownRemark node creation from gatsby-transformer-remark,
 *    adding slug, series, tag fields. These nodes are children of their
 *    associated File node.
 */
exports.onCreateNode = ({ node, boundActionCreators, getNode }) => {
  const { createNodeField } = boundActionCreators

  if (node.internal.type === 'File') {
    // Handle File nodes in post format

    const parsedFilePath = path.parse(node.absolutePath)
    const splitDir = parsedFilePath.dir.split('---')
    if (parsedFilePath.ext === '.md' && splitDir.length === 2) {
      // Add custom url pathname for blog posts.

      // Determine slug from directory name
      // The date prefix is not used (only for file browser order)
      // Example: 2017-04-01---foo-bar
      const slug = splitDir[1]
      const slugPath = slugToPath(slug)

      createNodeField({
        node,
        name: 'slugWithPath',
        value: { slug, path: slugPath },
      })
    }
  } else if (node.internal.type === 'MarkdownRemark' && typeof node.slug === 'undefined') {
    // Handle MarkdownRemark nodes (once)

    // Copy slug from parent node (File) to this MarkdownRemark node
    const fileNode = getNode(node.parent)
    const fileNodeSlug = _.get(fileNode, 'fields.slugWithPath')
    if (fileNodeSlug) {
      createNodeField({
        node,
        name: 'slugWithPath',
        value: fileNodeSlug,
      })

      // attach series slug
      const seriesSlug = postSlugToSeries[fileNodeSlug.slug]
      createNodeField({
        node,
        name: 'seriesSlug',
        value: seriesSlug || NOT_IN_SERIES,
      })
    }

    // Since tag => tag path conversion involves some complexity (i.e. _.kebabCase),
    // add a tagsWithPaths field to the MarkdownRemark node with (tag, path) maps
    if (node.frontmatter.tags) {
      const tagsWithPaths = node.frontmatter.tags.map((tag) => ({ tag, path: tagToPath(tag) }))

      createNodeField({
        node,
        name: 'tagsWithPaths',
        value: tagsWithPaths,
      })
    }
  }
}

exports.modifyWebpackConfig = ({ config, stage }) => {
  // Import from src root without leading paths
  config.merge({
    resolve: {
      root: path.resolve(__dirname, './src'),
      extensions: ['', '.js', '.jsx', '.json'],
    },
  })

  if (stage === 'build-javascript') {
    // Add Lodash plugin
    config.plugin('Lodash', webpackLodashPlugin, null)
  }

  return config
}
