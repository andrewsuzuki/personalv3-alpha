var path = require('path')

module.exports = {
  parser: 'babel-eslint',
  extends: 'airbnb',
  settings: {
    'import/resolver': {
      webpack: {
        config: {
          resolve: {
            root: path.resolve(__dirname, './src'),
            extensions: ['', '.js', '.jsx', '.json'],
          },
        },
      },
    },
  },
  rules: {
    indent: [2, 2, { SwitchCase: 1 }],
    'no-console': [0],
    'func-names': [0],
    semi: [2, 'never'],
    'no-extra-semi': [2],
    'arrow-parens': [2, 'always'],
    'space-before-function-paren': [2, 'always'],
    'no-else-return': [0],
    'space-infix-ops': [0],
    'react/prefer-es6-class': [0],
    'react/prefer-stateless-function': [0],
    'react/jsx-filename-extension': [1, { extensions: ['.js', '.jsx'] }],
    'import/no-unresolved': [0],
    'global-require': [0],
  },
  globals: {
    __PREFIX_LINKS__: true,
    graphql: false,
    document: false,
    fetch: false,
    Headers: false,
  },
}
