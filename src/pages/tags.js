import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import styled from 'react-emotion'
import orderBy from 'lodash/orderBy'

import { PageHeading, H2, Content, Paragraph, Pill } from 'components'

import { colors } from 'theme'

import { tagToPath } from 'utils'

const PillWithSupContainer = styled.span`
  display: inline-block;
  margin-right: 9px;

  &:last-of-type {
    margin-right: 0;
  }
`

const PillSup = styled.sup`
  padding-left: 2px;
  font-size: 0.6rem;
  vertical-align: 8px;
  color: ${colors.subtle};
`

const tagSorter = [({ totalCount }) => totalCount]

class TagsPageRoute extends React.Component {
  render () {
    const allTags = this.props.data.allMarkdownRemark.group

    return (
      <div>
        <Helmet title="All Tags" />
        <div>
          <PageHeading>
            <H2>All Tags</H2>
          </PageHeading>
          <Content>
            <Paragraph center>
              {orderBy(allTags, tagSorter, ['desc']).map((tag) => (
                <PillWithSupContainer key={tag.fieldValue}>
                  <Pill to={tagToPath(tag.fieldValue)} noMargin title={`Tag: ${tag.fieldValue}`}>
                    {tag.fieldValue}
                  </Pill>
                  <PillSup>{tag.totalCount}</PillSup>
                </PillWithSupContainer>
              ))}
            </Paragraph>
          </Content>
        </div>
      </div>
    )
  }
}

TagsPageRoute.propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      group: PropTypes.arrayOf(
        PropTypes.shape({
          fieldValue: PropTypes.string.isRequired,
          totalCount: PropTypes.number.isRequired,
        }),
      ).isRequired,
    }).isRequired,
  }).isRequired,
}

export default TagsPageRoute

export const pageQuery = graphql`
  query TagsQuery {
    allMarkdownRemark(limit: 2000, filter: { frontmatter: { draft: { ne: true } } }) {
      group(field: frontmatter___tags) {
        fieldValue
        totalCount
      }
    }
  }
`
