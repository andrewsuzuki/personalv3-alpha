---
category: uncategorized
title: Introduction
subtitle: My first foray into blogging
date: "2017-10-09T13:00:00.000Z"
tags:
  - me
  - meta
quick: false
draft: false
---

I'm excited to start blogging! So much of the content I consume daily is on others' personal blogs, and I don't want to be too late to the party. More soon.

### Site Implementation

This is a static website built with Facebook's [React](https://reactjs.org) library, leveraging the clever loading and data-fetching mechanics of [Gatsby](https://gatsbyjs.org). It's super slick.

The source code and other details can be found at its [Github repository](https://github.com/andrewsuzuki/andrewsuzuki.github.io).

Excuse this wireframe of a site as I roll features and content out incrementally!
