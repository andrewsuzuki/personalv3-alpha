import React from 'react'
import Helmet from 'react-helmet'

import {
  PageHeading,
  Content,
  PostList,
  H2,
  H4,
  Paragraph,
  Blurb,
  Breaks,
  ButtonLink,
} from 'components'

import { propTypes as postsPropTypes } from './posts'

class IndexRoute extends React.Component {
  render () {
    return (
      <div>
        <Helmet title="Home" />
        <PageHeading>
          <H2>Welcome!</H2>
        </PageHeading>
        <Content>
          <Paragraph bigger>
            <Blurb />
          </Paragraph>
          <Paragraph center>
            <ButtonLink to="/about" size="small" brand="default">
              More about me
            </ButtonLink>
          </Paragraph>

          <H4>Recent Posts</H4>
          <Breaks size={1} />
          <PostList
            posts={this.props.data.allMarkdownRemark.edges.map((post) => ({
              title: post.node.frontmatter.title,
              subtitle: post.node.frontmatter.subtitle,
              slugPath: post.node.fields.slugWithPath.path,
              date: post.node.frontmatter.date,
              category: post.node.frontmatter.category,
              tagsWithPaths: post.node.fields.tagsWithPaths,
              quick: post.node.frontmatter.quick,
            }))}
          />
          <Paragraph center>
            <ButtonLink to="/posts" size="small" brand="default">
              More Posts
            </ButtonLink>
          </Paragraph>
        </Content>
      </div>
    )
  }
}

IndexRoute.propTypes = postsPropTypes

export default IndexRoute

export const pageQuery = graphql`
  query HomeIndexQuery {
    allMarkdownRemark(
      limit: 5
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { draft: { ne: true } } }
    ) {
      edges {
        node {
          fields {
            slugWithPath {
              path
            }
            tagsWithPaths {
              tag
              path
            }
          }
          frontmatter {
            title
            subtitle
            date(formatString: "MMM DD YYYY")
            category
            quick
          }
        }
      }
    }
  }
`
