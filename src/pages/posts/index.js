import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import { PageHeading, PostList, Content, H2 } from 'components'

class BlogIndexRoute extends React.Component {
  render () {
    return (
      <div>
        <Helmet title="Home" />
        <PageHeading>
          <H2>Posts</H2>
        </PageHeading>
        <Content>
          <PostList
            posts={this.props.data.allMarkdownRemark.edges.map((post) => ({
              title: post.node.frontmatter.title,
              subtitle: post.node.frontmatter.subtitle,
              slugPath: post.node.fields.slugWithPath.path,
              date: post.node.frontmatter.date,
              category: post.node.frontmatter.category,
              tagsWithPaths: post.node.fields.tagsWithPaths,
              quick: post.node.frontmatter.quick,
            }))}
          />
        </Content>
      </div>
    )
  }
}

// also used by src/index (home page)
export const propTypes = {
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            fields: PropTypes.shape({
              slugWithPath: PropTypes.shape({
                path: PropTypes.string.isRequired,
              }).isRequired,
              tagsWithPaths: PropTypes.arrayOf(
                PropTypes.shape({
                  tag: PropTypes.string.isRequired,
                  path: PropTypes.string.isRequired,
                }),
              ),
            }).isRequired,
            frontmatter: PropTypes.shape({
              title: PropTypes.string.isRequired,
              subtitle: PropTypes.string,
              date: PropTypes.string.isRequired,
              category: PropTypes.string,
              quick: PropTypes.bool,
            }).isRequired,
          }).isRequired,
        }),
      ).isRequired,
    }).isRequired,
  }).isRequired,
}

BlogIndexRoute.propTypes = propTypes

export default BlogIndexRoute

export const pageQuery = graphql`
  query BlogIndexQuery {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { draft: { ne: true } } }
    ) {
      edges {
        node {
          fields {
            slugWithPath {
              path
            }
            tagsWithPaths {
              tag
              path
            }
          }
          frontmatter {
            title
            subtitle
            date(formatString: "MMM DD YYYY")
            category
            quick
          }
        }
      }
    }
  }
`
