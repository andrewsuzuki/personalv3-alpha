import React from 'react'
import Helmet from 'react-helmet'

import { PageHeading, Content, H2, Paragraph } from 'components'

export default class WorkRoute extends React.Component {
  render () {
    return (
      <div>
        <Helmet title="Work" />
        <PageHeading>
          <H2>Work</H2>
        </PageHeading>
        <Content>
          <Paragraph center>Coming soon! 🚧</Paragraph>
        </Content>
      </div>
    )
  }
}
