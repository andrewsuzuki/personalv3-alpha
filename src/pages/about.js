import React from 'react'
import Helmet from 'react-helmet'

import { PageHeading, Content, H2, Paragraph, Blurb, ButtonLink, Breaks } from 'components'

export default class AboutRoute extends React.Component {
  render () {
    return (
      <div>
        <Helmet title="About" />
        <PageHeading>
          <H2>About</H2>
        </PageHeading>
        <Content>
          <Paragraph>
            <Blurb />
          </Paragraph>
          <Paragraph>
            I am building my new site here incrementally, so this page is a work in progress. If
            you'd like to learn more, don't hesistate to send me an email!
          </Paragraph>
          <Breaks size={1} />
          <Paragraph center>
            <ButtonLink to="/contact" title="Contact me" brand="success" size="medium">
              Contact Me
            </ButtonLink>{' '}
          </Paragraph>
        </Content>
      </div>
    )
  }
}
