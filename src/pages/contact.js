import React from 'react'
import Helmet from 'react-helmet'

import { PageHeading, Content, H2, Paragraph, Flex, ContactForm } from 'components'

export default class ContactRoute extends React.Component {
  render () {
    return (
      <div>
        <Helmet title="Contact" />
        <PageHeading>
          <H2>Contact</H2>
        </PageHeading>
        <Content>
          <Paragraph>
            If you have a prospective job or you'd just like to say hello, feel free to drop me a
            line using the form below. I'll respond as soon as possible. Thanks!
          </Paragraph>
          <Flex halign="center">
            <ContactForm />
          </Flex>
        </Content>
      </div>
    )
  }
}
