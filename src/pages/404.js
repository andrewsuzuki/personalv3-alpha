import React from 'react'
import Helmet from 'react-helmet'

import { PageHeading, Content, H2, Paragraph, Link } from 'components'

export default class FourOFour extends React.Component {
  render () {
    return (
      <div>
        <Helmet title="404 Page Not Found" />
        <PageHeading>
          <H2>404 Page Not Found</H2>
        </PageHeading>
        <Content>
          <Paragraph>
            Could not find page. <Link to="/">Click here</Link> to go home.
          </Paragraph>
        </Content>
      </div>
    )
  }
}
