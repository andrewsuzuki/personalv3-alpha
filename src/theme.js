/**
 * Theme
 */

const colors = {
  text: '#1a1a1a', // very dark grey / black substitute
  subtle: '#6d6d6d', // light grey for text
  subtleOnDark: '#999999',
  divider: '#ececec', // very light grey
  formBorder: '#cccccc', // light grey
  white: '#ffffff',
  black: '#000000',

  // brand
  brandDefault: '#f3f3f3',
  brandPrimary: '#0074d9',
  brandSuccess: '#09bc8a',

  link: '#0074d9',
  linkHover: '#004cd9',

  pillLinkBackground: '#f3f3f3',
  pillLinkBackgroundActive: '#0074d9',

  spinnerBase: 'rgba(0,0,0,0.2)',
  spinnerNotch: 'rgba(0,0,0,1)',

  success: 'green',
  error: 'red',

  inlineCodeText: '#bd4147',
  inlineCodeBackground: '#f7f7f9',

  tableHeadingBackgroundAndDivider: '#eceeef',
  tableHeadingText: '#464a4c',

  // Use utils.categoryToPillTheme to get
  categoryPillThemes: {
    uncategorized: {
      color: 'white',
      colorHover: 'white',
      background: '#666666',
      backgroundHover: '#565656',
    },
    programming: {
      color: 'white',
      colorHover: 'white',
      background: '#37718e',
      backgroundHover: '#417d9b',
    },
    cycling: {
      color: 'white',
      colorHover: 'white',
      background: '#f896d8',
      backgroundHover: '#ffa3e1',
    },
    music: {
      color: 'white',
      colorHover: 'white',
      background: '#09bc8a',
      backgroundHover: '#17d19c',
    },
  },
}

const sizes = {
  xsmall: { min: 0, max: 599 },
  small: { min: 600, max: 779 },
  medium: { min: 780, max: 979 },
  large: { min: 980, max: 1279 },
  xlarge: { min: 1280, max: 1339 },
  xxlarge: { min: 1340, max: Infinity },

  // Sidebar/nav related tweakpoints
  largerSidebar: { min: 1100, max: 1339 },
  sidebarFixed: { min: 2000, max: Infinity },
}

const media = {
  between (smallKey, largeKey, excludeLarge = false) {
    if (excludeLarge) {
      return `@media (min-width: ${sizes[smallKey].min}px) and (max-width: ${sizes[largeKey].min -
        1}px)`
    } else if (sizes[largeKey].max === Infinity) {
      return `@media (min-width: ${sizes[smallKey].min}px)`
    } else {
      return `@media (min-width: ${sizes[smallKey].min}px) and (max-width: ${sizes[largeKey]
        .max}px)`
    }
  },

  greaterThan (key) {
    return `@media (min-width: ${sizes[key].min}px)`
  },

  lessThan (key) {
    return `@media (max-width: ${sizes[key].min - 1}px)`
  },

  size (key) {
    const size = sizes[key]

    if (size.min == null) {
      return media.lessThan(key)
    } else if (size.max == null) {
      return media.greaterThan(key)
    } else {
      return media.between(key, key)
    }
  },
}

const fonts = {
  standard:
    '-apple-system, BlinkMacSystemFont, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", sans-serif',
  headingSerif: '"leitura-news", serif', // from typekit
  monospace: '"Consolas", "Bitstream Vera Sans Mono", "Courier New", Courier, monospace',
}

const sharedStyles = {}

export default {
  colors,
  media,
  fonts,
  sharedStyles,
}
