import React from 'react'
import memoize from 'lodash/memoize'
import kebabCase from 'lodash/kebabCase'
import omit from 'lodash/omit'
import tinycolor from 'tinycolor2'

import theme from 'theme'

export const categoryToPath = memoize((category) => `/category/${kebabCase(category)}`)

export const tagToPath = memoize((tag) => `/tags/${kebabCase(tag)}`)

export const categoryToPillTheme = (category) => theme.colors.categoryPillThemes[category]

export const removePropsHOC = (component, removeList) => (props) =>
  React.createElement(component, omit(props, removeList))

// Color utils

export const darken = memoize(
  (color, amount) =>
    tinycolor(color)
      .darken(amount)
      .toString(),
  (color, amount) => `${color}-${amount || ''}`,
)
