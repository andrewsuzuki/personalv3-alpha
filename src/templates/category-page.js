import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import capitalize from 'lodash/capitalize'

import {
  PageHeading,
  Pretitle,
  H2,
  Content,
  PostList,

  // see TODO below
  // ButtonLink,
  // Breaks,
  // HorizontalRule,
  // Paragraph,
} from 'components'

class CategoryRoute extends React.Component {
  render () {
    const category = this.props.pathContext.category
    const categoryCapitalized = capitalize(this.props.pathContext.category)
    const posts = this.props.data.allMarkdownRemark.edges
    const totalCount = this.props.data.allMarkdownRemark.totalCount

    return (
      <div>
        <Helmet title={`Category: ${category}`} />
        <PageHeading>
          <Pretitle>
            Category with {totalCount} post{totalCount > 1 && 's'}
          </Pretitle>
          <H2>{categoryCapitalized}</H2>
        </PageHeading>
        <Content>
          <PostList
            posts={posts.map((post) => ({
              title: post.node.frontmatter.title,
              subtitle: post.node.frontmatter.subtitle,
              slugPath: post.node.fields.slugWithPath.path,
              date: post.node.frontmatter.date,
              category: post.node.frontmatter.category,
              tagsWithPaths: post.node.fields.tagsWithPaths,
              quick: post.node.frontmatter.quick,
            }))}
          />
          {/* TODO: page with all categories */}
          {/* <HorizontalRule />
          <Breaks size={1} />
          <Paragraph center>
            <ButtonLink to="/tags">Browse all categories</ButtonLink>
          </Paragraph> */}
        </Content>
      </div>
    )
  }
}

CategoryRoute.propTypes = {
  pathContext: PropTypes.shape({
    category: PropTypes.string.isRequired,
  }).isRequired,
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            fields: PropTypes.shape({
              slugWithPath: PropTypes.shape({
                path: PropTypes.string.isRequired,
              }).isRequired,
              tagsWithPaths: PropTypes.arrayOf(
                PropTypes.shape({
                  tag: PropTypes.string.isRequired,
                  path: PropTypes.string.isRequired,
                }),
              ),
            }).isRequired,
            frontmatter: PropTypes.shape({
              title: PropTypes.string.isRequired,
              subtitle: PropTypes.string,
              date: PropTypes.string.isRequired,
              category: PropTypes.string,
              quick: PropTypes.bool,
            }).isRequired,
          }).isRequired,
        }),
      ).isRequired,
    }).isRequired,
  }).isRequired,
}

export default CategoryRoute

export const pageQuery = graphql`
  query CategoryPage($category: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { category: { eq: $category }, draft: { ne: true } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slugWithPath {
              path
            }
            tagsWithPaths {
              tag
              path
            }
          }
          frontmatter {
            title
            subtitle
            date(formatString: "MMM DD YYYY")
            category
            quick
          }
        }
      }
    }
  }
`
