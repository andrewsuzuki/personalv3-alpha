import React from 'react'

import styled from 'react-emotion'
import { connect } from 'react-redux'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import get from 'lodash/get'

import {
  PageHeading,
  H2,
  Pretitle,
  Subtitle,
  Content,
  HtmlStyledBasic,
  SubtleDate,
  CategoryAndTagPills,
  QuickLightning,
} from 'components'

const PageHeadingPartHideEmpty = styled.div`
  margin-top: 0.5em;

  &:empty {
    display: none;
  }
`

class BlogPostRoute extends React.Component {
  componentDidMount () {
    this.setCurrentPostCategory()
  }

  componentDidUpdate () {
    this.setCurrentPostCategory()
  }

  componentWillUnmount () {
    // nullify category
    this.setCurrentPostCategory(true)
  }

  setCurrentPostCategory = (nullify = false) => {
    const category = get(this.props, 'data.markdownRemark.frontmatter.category')

    if (!nullify && category) {
      this.props.setCurrentPostCategory(category)
    } else {
      this.props.setCurrentPostCategory(null)
    }
  }

  render () {
    const post = this.props.data.markdownRemark

    // eslint-disable-next-line react/no-danger
    const postContent = <HtmlStyledBasic dangerouslySetInnerHTML={{ __html: post.html }} />

    return (
      <div>
        <Helmet
          title={post.frontmatter.title}
          meta={[{ name: 'description', content: post.excerpt }]}
        />
        <PageHeading>
          {post.frontmatter.quick && (
            <Pretitle>
              <QuickLightning smaller />&nbsp;Quick read
            </Pretitle>
          )}
          <H2>{post.frontmatter.title}</H2>
          <Subtitle>{post.frontmatter.subtitle}</Subtitle>
          <PageHeadingPartHideEmpty>
            <CategoryAndTagPills
              category={post.frontmatter.category}
              tagsWithPaths={post.fields.tagsWithPaths}
            />
          </PageHeadingPartHideEmpty>
          <PageHeadingPartHideEmpty>
            <SubtleDate>{post.frontmatter.date}</SubtleDate>
          </PageHeadingPartHideEmpty>
        </PageHeading>
        <Content>{postContent}</Content>
      </div>
    )
  }
}

BlogPostRoute.propTypes = {
  setCurrentPostCategory: PropTypes.func.isRequired,
  data: PropTypes.shape({
    markdownRemark: PropTypes.shape({
      html: PropTypes.string.isRequired,
      fields: PropTypes.shape({
        tagsWithPaths: PropTypes.arrayOf(
          PropTypes.shape({
            tag: PropTypes.string.isRequired,
            path: PropTypes.string.isRequired,
          }),
        ),
      }).isRequired,
      frontmatter: PropTypes.shape({
        title: PropTypes.string.isRequired,
        subtitle: PropTypes.string,
        date: PropTypes.string.isRequired,
        category: PropTypes.string,
        quick: PropTypes.bool,
      }).isRequired,
    }).isRequired,
  }).isRequired,
}

export default connect(
  (/* state */) => ({}),
  (dispatch) => ({
    setCurrentPostCategory: (category) =>
      dispatch({ type: 'SET_CURRENT_POST_CATEGORY', payload: category }),
  }),
)(BlogPostRoute)

export const pageQuery = graphql`
  query BlogPostBySlug($slug: String!) {
    markdownRemark(fields: { slugWithPath: { slug: { eq: $slug } } }) {
      html
      fields {
        tagsWithPaths {
          tag
          path
        }
      }
      frontmatter {
        title
        subtitle
        date(formatString: "MMMM DD, YYYY")
        category
        quick
      }
    }
  }
`
