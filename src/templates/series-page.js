import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'

import { PageHeading, Pretitle, H2, Content, PostList } from 'components'

class SeriesRoute extends React.Component {
  render () {
    const { title } = this.props.pathContext.series
    const posts = this.props.data.allMarkdownRemark.edges
    const totalCount = this.props.data.allMarkdownRemark.totalCount

    return (
      <div>
        <Helmet title={`Series: ${title}`} />
        <PageHeading>
          <Pretitle>
            Series with {totalCount} part{totalCount > 1 && 's'}
          </Pretitle>
          <H2>{title}</H2>
        </PageHeading>
        <Content>
          <PostList
            posts={posts.map((post) => ({
              title: post.node.frontmatter.title,
              subtitle: post.node.frontmatter.subtitle,
              slugPath: post.node.fields.slugWithPath.path,
              date: post.node.frontmatter.date,
              category: post.node.frontmatter.category,
              tagsWithPaths: post.node.fields.tagsWithPaths,
              quick: post.node.frontmatter.quick,
            }))}
          />
        </Content>
      </div>
    )
  }
}

SeriesRoute.propTypes = {
  pathContext: PropTypes.shape({
    series: PropTypes.shape({
      title: PropTypes.string.isRequired,
    }),
  }).isRequired,
  data: PropTypes.shape({
    allMarkdownRemark: PropTypes.shape({
      totalCount: PropTypes.number.isRequired,
      edges: PropTypes.arrayOf(
        PropTypes.shape({
          node: PropTypes.shape({
            fields: PropTypes.shape({
              slugWithPath: PropTypes.shape({
                path: PropTypes.string.isRequired,
              }).isRequired,
              tagsWithPaths: PropTypes.arrayOf(
                PropTypes.shape({
                  tag: PropTypes.string.isRequired,
                  path: PropTypes.string.isRequired,
                }),
              ),
            }).isRequired,
            frontmatter: PropTypes.shape({
              title: PropTypes.string.isRequired,
              subtitle: PropTypes.string,
              date: PropTypes.string.isRequired,
              category: PropTypes.string,
              quick: PropTypes.bool,
            }).isRequired,
          }).isRequired,
        }),
      ).isRequired,
    }).isRequired,
  }).isRequired,
}

export default SeriesRoute

export const pageQuery = graphql`
  query SeriesQuery($seriesSlug: String) {
    allMarkdownRemark(
      limit: 2000
      sort: { fields: [frontmatter___date], order: ASC }
      filter: { fields: { seriesSlug: { eq: $seriesSlug } } }
    ) {
      totalCount
      edges {
        node {
          fields {
            slugWithPath {
              path
            }
            tagsWithPaths {
              tag
              path
            }
          }
          frontmatter {
            title
            subtitle
            date(formatString: "MMM DD YYYY")
            category
            quick
          }
        }
      }
    }
  }
`
