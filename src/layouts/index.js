import React from 'react'
import PropTypes from 'prop-types'
import Helmet from 'react-helmet'
import styled from 'react-emotion'
import { Provider } from 'react-redux'

import { colors } from 'theme'

import { WrapperSlim, Header, Footer } from 'components'

import injectGlobalStyles from 'css/injectGlobalStyles'

import store from 'store'

// Inject global styles and fonts
injectGlobalStyles()

// Set up sticky footer; body font color
const Container = styled.div`
  display: flex;
  min-height: 100vh;
  flex-direction: column;

  color: ${colors.text};
`

const Content = styled.div`
  flex: 1;
`

class MainLayout extends React.Component {
  render () {
    return (
      <Provider store={store}>
        <Container>
          <Helmet defaultTitle="Andrew Suzuki" titleTemplate="Andrew Suzuki | %s" />
          <Content>
            <Header />
            <WrapperSlim>{this.props.children()}</WrapperSlim>
          </Content>
          <Footer />
        </Container>
      </Provider>
    )
  }
}

MainLayout.propTypes = {
  children: PropTypes.func.isRequired,
}

export default MainLayout
