/**
 * Redux store
 *
 * Using redux to keep track of the category of the post currently being viewed.
 * This is used to set the active state of the category links in the header.
 */

import { compose, createStore, combineReducers, applyMiddleware } from 'redux'

const reducer = combineReducers({
  currentPostCategory: (state = null, action) => {
    switch (action.type) {
      case 'SET_CURRENT_POST_CATEGORY':
        return action.payload
      default:
        return state
    }
  },
})

const buildStore = compose(applyMiddleware())(createStore)

export function configureStore (initialState) {
  const store = buildStore(reducer, initialState)

  return store
}

export default configureStore()
