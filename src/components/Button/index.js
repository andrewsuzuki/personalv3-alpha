import PropTypes from 'prop-types'
import styled from 'react-emotion'

import { colors } from 'theme'

import { darken } from 'utils'

const sizeToFontSize = {
  small: '0.8rem',
  medium: '1rem',
  large: '1.3rem',
}

const brandToStyle = {
  default: { background: colors.brandDefault, color: colors.text },
  primary: { background: colors.brandPrimary, color: colors.white },
  success: { background: colors.brandSuccess, color: colors.white },
}

export const style = [
  {
    display: 'inline-block',
    textDecoration: 'none',
    border: 0,
    borderRadius: 3,
    padding: '0.5em 0.75em',
    fontWeight: 400,
    color: colors.white,
    cursor: 'pointer',
  },
  // Size variants
  ({ size }) => ({ fontSize: sizeToFontSize[size] }),
  // Brand variants
  ({ brand }) => {
    const s = brandToStyle[brand]

    return {
      ...s,
      ':hover': {
        background: darken(s.background, 2),
      },
    }
  },
  // Disabled
  ({ disabled }) => disabled && { cursor: 'not-allowed', opacity: 0.5 },
]

const Button = styled.button(...style)

export const buttonPropTypes = {
  size: PropTypes.oneOf(Object.keys(sizeToFontSize)),
  brand: PropTypes.oneOf(Object.keys(brandToStyle)),
  disabled: PropTypes.bool,
}
Button.propTypes = buttonPropTypes

export const buttonDefaultProps = {
  size: 'small',
  brand: 'default',
  disabled: false,
}
Button.defaultProps = buttonDefaultProps

export default Button
