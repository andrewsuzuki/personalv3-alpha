import React from 'react'
import PropTypes from 'prop-types'
import { field } from 'neoform'
import { fieldValidation } from 'neoform-validation'
import capitalize from 'lodash/capitalize'

import { Label, ErrorMessage, TextField, TextareaField } from './supportComponents'

const renderError = (status, message) => {
  if (status !== false) {
    return null
  }

  return <ErrorMessage>{message}</ErrorMessage>
}

const MyInput = ({
  type,
  name,
  value = '',
  onChange,
  validate,
  validationStatus,
  validationMessage,
  ...props
}) => {
  const onChangeReal = (e) => {
    onChange(e.target.value)
    setTimeout(validate, 100)
  }

  const component = type === 'textarea' ? TextareaField : TextField

  return (
    <Label>
      <span>{capitalize(name)}</span>
      {React.createElement(component, {
        ...props,
        type,
        name,
        value,
        onBlur: validate,
        onChange: onChangeReal,
        hasError: validationStatus === false,
      })}
      {renderError(validationStatus, validationMessage)}
    </Label>
  )
}

MyInput.propTypes = {
  type: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  validate: PropTypes.func.isRequired,
  validationStatus: PropTypes.bool,
  validationMessage: PropTypes.string,
}

MyInput.defaultProps = {
  validationStatus: undefined,
  validationMessage: undefined,
}

export default field(fieldValidation(MyInput))
