import { css } from 'emotion'
import styled from 'react-emotion'

import { colors, media } from 'theme'

const commonFieldStyle = css`
  font-size: 1rem;
  font-weight: 400;
  border-radius: 3px;
  padding: 8px 10px;
  border-color: ${colors.formBorder};
  border-width: 1px;
  border-style: solid;

  &:disabled {
    cursor: not-allowed;
    background-color: ${colors.formBorder};
    opacity: 0.6;
  }
`

const commonFieldWidthStyle = css`
  width: 100%;

  ${media.greaterThan('small')} {
    width: 400px;
  }
`

export const Label = styled.label`
  ${commonFieldWidthStyle};

  display: block;
  float: left;
  clear: both;
  margin-top: 1rem;

  > span:first-of-type {
    ${commonFieldWidthStyle};

    display: block;
    margin-top: 5px;
    margin-bottom: 3px;
    font-size: 1rem;
  }
`

export const ErrorMessage = styled.span`
  display: block;
  color: ${colors.error};
  font-size: 0.8rem;
  margin-top: 2px;
`

export const TextField = styled.input`
  ${commonFieldStyle};
  ${commonFieldWidthStyle};

  ${({ hasError }) =>
    hasError &&
    css`
      border-color: ${colors.error};
    `};
`

export const TextareaField = styled.textarea`
  ${commonFieldStyle};
  ${commonFieldWidthStyle};

  resize: vertical;
  min-height: 150px;

  ${({ hasError }) =>
    hasError &&
    css`
      border-color: ${colors.error};
    `};
`

export const FormErrorMessage = styled.span`
  display: block;
  margin-top: 1rem;
  font-size: 0.9rem;
  color: ${colors.error};
`

export const SuccessMessage = styled.span`
  display: block;
  margin-top: 1rem;
  color: ${colors.success};
`
