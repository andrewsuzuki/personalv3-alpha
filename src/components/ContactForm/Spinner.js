import styled from 'react-emotion'

import { spin } from 'css/keyframes'

import { colors } from 'theme'

const Spinner = styled.div`
  font-size: 2.5px;
  position: relative;
  text-indent: -9999em;
  border-top: 1.2em solid ${colors.spinnerBase};
  border-right: 1.2em solid ${colors.spinnerBase};
  border-bottom: 1.2em solid ${colors.spinnerBase};
  border-left: 1.2em solid ${colors.spinnerNotch};
  transform: translateZ(0);
  animation: ${spin} 800ms infinite linear;

  &,
  &::after {
    border-radius: 50%;
    width: 10em;
    height: 10em;
  }
`

export default Spinner
