import React from 'react'
import { getValue, setValue } from 'neoform-plain-object-helpers'
import noop from 'lodash/noop'

import Link from 'components/Link'
import Paragraph from 'components/Paragraph'

import Form from './Form'
import sendEmail from './sendEmail'
import { SuccessMessage } from './supportComponents'

class ContactForm extends React.Component {
  constructor (props) {
    super(props)

    // Set initial state
    this.reset()
  }

  onChange = (name, value) => {
    this.setState((prevState) => setValue(prevState, name, value))
  }

  onSubmit = () => {
    this.setState({
      loading: true,
      status: 1,
    })

    const values = {
      name: this.state.name,
      email: this.state.email,
      message: this.state.message,
    }

    sendEmail(values)
      .then((response) => {
        if (response.status !== 200) {
          return Promise.reject(response.status)
        }

        return Promise.resolve()
      })
      .then(() => {
        // Success

        this.setState({
          status: 2,
        })
      })
      .catch((codeOrTypeError) => {
        // Error

        this.setState({
          status: codeOrTypeError instanceof TypeError ? 3 : 4,
        })
      })
  }

  reset = () => {
    const initial = {
      // Form values
      name: '',
      email: '',
      message: '',

      /**
       * Status enum:
       *
       * 0 = idle
       * 1 = email send request active (show loading state)
       * 2 = Success (show success message, hide form)
       * 3 = Fail -- network error
       * 4 = Fail -- server/http error
       */
      status: 0,
    }

    if (this.state) {
      this.setState(initial)
    } else {
      this.state = initial
    }
  }

  render () {
    const { status } = this.state

    if (status === 2) {
      return (
        <div>
          <Paragraph>
            <SuccessMessage>Your message was sent successfully.</SuccessMessage>
          </Paragraph>
          <Paragraph>
            <Link onClick={this.reset}>Click here</Link> to send another message.
          </Paragraph>
        </div>
      )
    }

    return (
      <Form
        data={this.state}
        getValue={getValue}
        onChange={this.onChange}
        onInvalid={noop}
        onSubmit={this.onSubmit}
        status={status}
      />
    )
  }
}

export default ContactForm
