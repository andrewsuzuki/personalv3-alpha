import React from 'react'
import PropTypes from 'prop-types'
import { form } from 'neoform'
import { formValidation } from 'neoform-validation'
import baseEmailValidator from 'email-validator'

import Clear from 'components/Clear'
import FlexCenterAdjacent from 'components/FlexCenterAdjacent'
import Button from 'components/Button'

import Input from './Input'
import Spinner from './Spinner'

import { Submit, FormErrorMessage } from './supportComponents'

const requiredValidator = (value) => {
  if (typeof value === 'undefined' || value === null || value === '') {
    return Promise.reject('required')
  }

  return Promise.resolve('valid')
}

const emailValidator = (value) =>
  requiredValidator(value).then(() => {
    if (!baseEmailValidator.validate(value)) {
      return Promise.reject('not a valid email')
    }

    return Promise.resolve('valid')
  })

const MyForm = ({ validate, validationStatus, onSubmit, onInvalid, status }) => (
  <form
    onSubmit={(e) => {
      e.preventDefault()
      validate(onSubmit, onInvalid)
    }}
  >
    <Input disabled={status === 1} type="text" name="name" validator={requiredValidator} />
    <Input disabled={status === 1} type="email" name="email" validator={emailValidator} />
    <Input disabled={status === 1} type="textarea" name="message" validator={requiredValidator} />

    <Clear />
    <br />
    <FlexCenterAdjacent>
      <div>
        <Button type="submit" brand="primary" size="medium" disabled={status === 1}>
          Send
        </Button>
      </div>
      {status === 1 && <Spinner />}
    </FlexCenterAdjacent>

    {validationStatus === false && (
      <FormErrorMessage>Please correct the errors before submitting.</FormErrorMessage>
    )}
    {status === 3 && (
      <FormErrorMessage>
        Network error: Make sure your internet is working correctly, then try sending again.
      </FormErrorMessage>
    )}
    {status === 4 && (
      <FormErrorMessage>
        Server error: There was a problem sending the message. Please try again in a few minutes.
      </FormErrorMessage>
    )}
  </form>
)

MyForm.propTypes = {
  validate: PropTypes.func.isRequired,
  validationStatus: PropTypes.bool.isRequired,
  onSubmit: PropTypes.func.isRequired,
  onInvalid: PropTypes.func.isRequired,
  status: PropTypes.number.isRequired,
}

export default form(formValidation(MyForm))
