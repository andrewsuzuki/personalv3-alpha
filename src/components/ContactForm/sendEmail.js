import { contactFormToEmail } from 'config'

const sendEmail = (formData) =>
  fetch(`https://formspree.io/${contactFormToEmail}`, {
    method: 'POST',
    mode: 'cors',
    /* eslint-disable quote-props */
    headers: new Headers({
      'Content-Type': 'application/json',
      Accept: 'application/json',
    }),
    /* eslint-enable quote-props */
    body: JSON.stringify({
      ...formData,
      _format: 'plain',
    }),
  })

export default sendEmail
