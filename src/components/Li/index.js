import styled from 'react-emotion'
import { css } from 'emotion'

export const style = css`
  & > p:first-child {
    display: inline-block;
  }
`

const Li = styled.li`
  ${style};
`

export default Li
