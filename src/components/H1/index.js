import styled from 'react-emotion'
import { css } from 'emotion'

export const style = css`
  font-size: 3rem;
  font-weight: bold;
  letter-spacing: 0.01em;
`

const H1 = styled.h1`
  ${style};
`

export default H1
