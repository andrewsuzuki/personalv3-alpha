import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'

import { Link, SubtleDate, CategoryAndTagPills, QuickLightning } from 'components'

import { colors, media } from 'theme'

/**
 * PostList
 * Date next to post title
 * Breaks below medium breakpoint
 */

const postPropType = PropTypes.shape({
  title: PropTypes.string.isRequired,
  subtitle: PropTypes.string,
  slugPath: PropTypes.string.isRequired,
  date: PropTypes.string.isRequired,
  category: PropTypes.string,
  tagsWithPaths: PropTypes.arrayOf(
    PropTypes.shape({
      tag: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
    }),
  ),
  quick: PropTypes.bool,
})

const Container = styled.section`
  width: 80%;
  margin: 0 auto;

  ${media.lessThan('medium')} {
    width: 100%;
  }
`

const PostContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: flex-start;

  margin-bottom: 2rem;

  ${media.lessThan('small')} {
    display: block;
  }
`

const Left = styled.div`
  line-height: 1;

  flex: none;
  width: 120px;
  margin-top: 0.15rem;

  ${media.lessThan('small')} {
    margin-top: 0;
    margin-bottom: 0.4rem;
  }
`

const Right = styled.div`
  font-size: 1.2rem;
  line-height: 1;

  ${media.lessThan('small')} {
    font-size: 1rem;

    margin-top: 0.2em;
  }
`

const Subtitle = styled.div`
  margin-top: 0.7em;
  font-size: 0.7em;
  font-style: italic;
  color: ${colors.subtle};

  &:empty {
    display: none;
  }
`

const PillContainer = styled.div`
  margin-top: 0.5em;

  &:empty {
    display: none;
  }
`

const Post = ({ post: { title, subtitle, category, date, slugPath, tagsWithPaths, quick } }) => (
  <PostContainer>
    <Left>
      <SubtleDate>{date}</SubtleDate>
    </Left>
    <Right>
      <Link to={slugPath}>{title}</Link>

      <Subtitle>{subtitle}</Subtitle>

      <PillContainer>
        <CategoryAndTagPills category={category} tagsWithPaths={tagsWithPaths} />
        {quick && (
          <span>
            &nbsp;<QuickLightning />
          </span>
        )}
      </PillContainer>
    </Right>
  </PostContainer>
)

Post.propTypes = {
  post: postPropType.isRequired,
}

const PostList = ({ posts }) => (
  <Container>{posts.map((post) => <Post key={post.slugPath} post={post} />)}</Container>
)

PostList.propTypes = {
  posts: PropTypes.arrayOf(postPropType).isRequired,
}

export default PostList
