import styled from 'react-emotion'

import { colors } from 'theme'

/**
 * Pretitle is intended to be placed as the first child of
 * PageHeading, before an H2, to identify what it is (i.e. "tag", or "category")
 */
const Pretitle = styled.span`
  display: block;

  text-align: center;
  font-size: 0.8rem;
  color: ${colors.subtle};

  margin-bottom: 0.4rem;

  &:empty {
    display: none;
  }
`

export default Pretitle
