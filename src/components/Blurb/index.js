import React from 'react'

import Link from 'components/Link'

const Blurb = () => (
  <span>
    I am a <Link to="/work">software engineer</Link> with a focus on the web. I also spend a lot of
    time <Link to="/category/cycling">cycling</Link> and studying{' '}
    <Link to="/category/music">music</Link>. I live in{' '}
    <Link href="https://en.wikipedia.org/wiki/New_Haven,_Connecticut">New Haven, Connecticut</Link>.
  </span>
)

export default Blurb
