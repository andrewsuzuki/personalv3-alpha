import { style as buttonStyleArgs, buttonPropTypes, buttonDefaultProps } from 'components/Button'

import linkComponentFactory from 'components/Link/linkComponentFactory'

export default linkComponentFactory(buttonStyleArgs, buttonPropTypes, buttonDefaultProps)
