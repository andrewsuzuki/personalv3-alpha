import styled from 'react-emotion'

const FlexCenterAdjacent = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;

  > div {
    margin-right: 15px;

    &::last-of-type {
      margin-right: 0;
    }
  }
`

export default FlexCenterAdjacent
