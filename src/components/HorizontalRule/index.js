import styled from 'react-emotion'
import { css } from 'emotion'

import { colors } from 'theme'

export const style = css`
  width: 80%;

  margin-top: 2em;
  margin-left: auto;
  margin-right: auto;

  border: 0 solid ${colors.divider};
  border-top-width: 1px;
`

const HorizontalRule = styled.hr`
  ${style};
`

export default HorizontalRule
