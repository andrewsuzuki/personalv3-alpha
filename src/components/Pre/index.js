import styled from 'react-emotion'
import { css } from 'emotion'

/**
 * NOTE:
 * Pre styling in markdown content is handled by Prismjs
 * (see css/prism)
 */

export const style = css`
  margin-top: 1em;
`

const Pre = styled.pre`
  ${style};
`

export default Pre
