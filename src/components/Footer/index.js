import React from 'react'
import styled from 'react-emotion'

import { colors, media } from 'theme'

import WrapperFull from 'components/WrapperFull'
import Link from 'components/Link'

const Container = styled.footer`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  margin-top: 3rem;
  border-top: 1px solid ${colors.divider};
  height: 70px;

  font-size: 0.9rem;

  ${media.lessThan('medium')} {
    height: 50px;
  }
`

const Footer = () => (
  <WrapperFull>
    <Container>
      <span>Andrew Suzuki</span>
      <span>
        <Link
          href="https://github.com/andrewsuzuki/andrewsuzuki.github.io"
          target="_blank"
          title="View the source code for this website"
        >
          Source
        </Link>
        &nbsp;&nbsp;&nbsp;&nbsp;
        <Link to="/contact">Contact</Link>
      </span>
    </Container>
  </WrapperFull>
)

export default Footer
