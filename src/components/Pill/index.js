import styled from 'react-emotion'
import PropTypes from 'prop-types'
import { css } from 'emotion'
import memoize from 'lodash/memoize'

import { removePropsHOC } from 'utils'

import { colors } from 'theme'

import Link from 'components/Link'

export const style = css`
  display: inline-block;
  border: 0;
  border-radius: 2px;
  padding: 0 0.83333em;
  max-width: 100%;
  overflow: hidden;
  text-decoration: none;
  line-height: 1.66667;
  font-size: 0.75rem;
  vertical-align: -0.25rem;

  background-color: ${colors.pillLinkBackground};
  font-weight: 600;

  text-transform: lowercase;

  margin-right: 3px;

  &:empty {
    display: none;
  }

  &:last-of-type {
    margin-right: 0;
  }
`

const themeToCss = memoize(
  (theme) =>
    theme &&
    css`
      color: ${theme.color};
      background: ${theme.background};
      ${theme.backgroundHover &&
        css`
          &:hover {
            background: ${theme.backgroundHover};
          }
        `};
      ${theme.colorHover &&
        css`
          &:hover {
            color: ${theme.colorHover};
          }
        `};
    `,
)

const Pill = styled(removePropsHOC(Link, ['noMargin']))`
  ${style};
  ${({ theme }) => themeToCss(theme)};
  ${({ noMargin }) =>
    noMargin &&
    css`
      margin-right: 0;
    `};
`

Pill.propTypes = {
  noMargin: PropTypes.bool,
  theme: PropTypes.shape({
    color: PropTypes.string.isRequired,
    background: PropTypes.string.isRequired,
    colorHover: PropTypes.string,
    backgroundHover: PropTypes.string,
  }),
}

Pill.defaultProps = {
  noMargin: false,
}

export default Pill
