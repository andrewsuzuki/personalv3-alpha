import styled from 'react-emotion'
import { css } from 'emotion'

export const style = css`
  list-style-position: inside;
  margin-top: 1em;
`

const Ol = styled.ol`
  ${style};
`

export default Ol
