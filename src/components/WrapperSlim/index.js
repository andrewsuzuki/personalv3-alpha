import styled from 'react-emotion'
import PropTypes from 'prop-types'

import { media } from 'theme'

const WrapperSlim = styled.div`
  margin: 0 auto;
  padding: 0 20px;
  width: 100%;

  ${media.greaterThan('medium')} {
    width: 740px;
  }
`

WrapperSlim.propTypes = {
  children: PropTypes.node.isRequired,
}

export default WrapperSlim
