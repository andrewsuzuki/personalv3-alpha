import styled from 'react-emotion'
import { css } from 'emotion'

import { colors } from 'theme'

export const style = css`
  margin-top: 1em;

  border-collapse: collapse;
  background-color: transparent;

  & th,
  & td {
    vertical-align: top;
    padding: 0.75rem;
    border-top: 1px solid ${colors.tableHeadingBackgroundAndDivider};

    &:not([align='right']) {
      text-align: left;
    }
  }

  & thead th {
    vertical-align: bottom;
    color: ${colors.tableHeadingText};
    background-color: ${colors.tableHeadingBackgroundAndDivider};
  }
`

const Table = styled.table`
  ${style};
`

export default Table
