import styled from 'react-emotion'
import PropTypes from 'prop-types'

import Clear from 'components/Clear'

const Breaks = styled(Clear)`
  height: ${({ size }) => `${size}rem`};
`

Breaks.propTypes = {
  size: PropTypes.number,
}

Breaks.defaultProps = {
  size: 1,
}

export default Breaks
