import styled from 'react-emotion'
import { css } from 'emotion'
import PropTypes from 'prop-types'

import { media } from 'theme'

export const style = css`
  margin-top: 1em;

  &:first-child {
    margin-top: 0;
  }
`

const Paragraph = styled.p`
  ${style};

  text-align: ${({ center }) => (center ? 'center' : 'left')};

  ${({ bigger }) =>
    bigger &&
    css`
      font-size: 1.5em;
      font-weight: 300;

      ${media.lessThan('medium')} {
        font-size: 1.2em;
      }
    `};
`

Paragraph.propTypes = {
  center: PropTypes.bool,
  bigger: PropTypes.bool,
}

Paragraph.defaultProps = {
  center: false,
  bigger: false,
}

export default Paragraph
