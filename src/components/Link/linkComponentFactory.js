import React from 'react'
import styled from 'react-emotion'
import PropTypes from 'prop-types'
import GatsbyLink from 'gatsby-link'

import { removePropsHOC } from 'utils'

/**
 * Hybrid component that switches between "native" a and gatsby-link (react-router) depending
 * on existence of the "to" prop. Supply a list of styles to be spread into styled (react-emotion).
 * Used by Link and ButtonLink. The prop name keys in extraPropTypes will be used to
 * strip non-native props before reaching the "native" components
 */
const linkComponentFactory = (styleArgs, extraPropTypes = {}, extraDefaultProps = {}) => {
  const removeProps = Object.keys(extraPropTypes)

  // NOTE: the removePropsHOC can be removed once emotion applies a native-attribute whitelist
  // see: https://github.com/emotion-js/emotion/issues/183
  const BaseNative = styled(removePropsHOC('a', removeProps))(...styleArgs)
  const BaseGatsby = styled(removePropsHOC(GatsbyLink, removeProps))(...styleArgs)

  const Link = (props) => React.createElement(props.to ? BaseGatsby : BaseNative, props)

  Link.propTypes = {
    to: PropTypes.string,
    ...extraPropTypes,
  }

  Link.defaultProps = {
    to: null,
    ...extraDefaultProps,
  }

  return Link
}

export default linkComponentFactory
