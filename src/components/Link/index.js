import { css } from 'emotion'

import { colors } from 'theme'

import linkComponentFactory from './linkComponentFactory'

export const style = css`
  cursor: pointer;
  text-decoration: none;
  color: ${colors.link};

  &:hover {
    text-decoration: none;
    color: ${colors.linkHover};
  }
`

export default linkComponentFactory([style])
