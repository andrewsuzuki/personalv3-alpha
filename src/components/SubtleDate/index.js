import styled from 'react-emotion'

import { fonts, colors } from 'theme'

const SubtleDate = styled.span`
  font-family: ${fonts.monospace};
  text-transform: uppercase;
  font-size: 0.8rem;
  color: ${colors.subtle};
`

export default SubtleDate
