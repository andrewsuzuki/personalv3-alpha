import React from 'react'
import PropTypes from 'prop-types'

import { categoryToPath, categoryToPillTheme } from 'utils'

import Pill from 'components/Pill'

const CategoryAndTagPills = ({ category, tagsWithPaths }) => {
  const hasCategory = category && category.length
  const hasTags = tagsWithPaths && tagsWithPaths.length

  if (!hasCategory && !hasTags) {
    return null
  }

  return (
    <span>
      {category && (
        <Pill
          to={categoryToPath(category)}
          theme={categoryToPillTheme(category)}
          title={`Category: ${category}`}
        >
          {category}
        </Pill>
      )}
      {tagsWithPaths &&
        tagsWithPaths.length > 0 &&
        tagsWithPaths.map(({ tag, path }) => (
          <Pill key={path} to={path} title={`Tag: ${tag}`}>
            {tag}
          </Pill>
        ))}
    </span>
  )
}

CategoryAndTagPills.propTypes = {
  category: PropTypes.string,
  tagsWithPaths: PropTypes.arrayOf(
    PropTypes.shape({
      tag: PropTypes.string.isRequired,
      path: PropTypes.string.isRequired,
    }),
  ),
}

CategoryAndTagPills.defaultProps = {
  category: null,
  tagsWithPaths: null,
}

export default CategoryAndTagPills
