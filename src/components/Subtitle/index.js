import styled from 'react-emotion'
import { css } from 'emotion'

import { media, colors } from 'theme'

export const style = css`
  display: block;

  text-align: center;
  font-size: 1rem;
  font-style: italic;
  color: ${colors.subtle};

  &:empty {
    display: none;
  }

  margin-top: 0.7rem;
  ${media.between('medium', 'large')} {
    margin-top: 0.5rem;
  }
  ${media.lessThan('medium')} {
    margin-top: 0.3rem;
  }
`

const Subtitle = styled.span`
  ${style};
`

export default Subtitle
