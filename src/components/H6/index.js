import styled from 'react-emotion'
import { css } from 'emotion'

import { media } from 'theme'

export const style = css`
  font-weight: 300;

  font-size: 1rem;
  ${media.between('small', 'large')} {
    font-size: 1rem;
  }
  ${media.lessThan('small')} {
    font-size: 1rem;
  }

  margin-top: 1em;

  &:first-child {
    margin-top: 0;
  }
`

const H6 = styled.h6`
  ${style};
`

export default H6
