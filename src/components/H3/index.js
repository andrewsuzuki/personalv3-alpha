import styled from 'react-emotion'
import { css } from 'emotion'

import { fonts, media } from 'theme'

export const style = css`
  text-align: center;

  font-family: ${fonts.headingSerif};
  font-weight: 300;
  line-height: 1.1;

  font-size: 2rem;
  ${media.between('small', 'large')} {
    font-size: 1.8rem;
  }
  ${media.lessThan('small')} {
    font-size: 1.6rem;
  }

  margin-top: 1em;

  &:first-child {
    margin-top: 0;
  }
`

const H3 = styled.h3`
  ${style};
`

export default H3
