import styled from 'react-emotion'
import { css } from 'emotion'

import { colors } from 'theme'

export const style = css`
  color: ${colors.inlineCodeText};
  background-color: ${colors.inlineCodeBackground};
  padding: 0.2rem 0.4rem;
  font-size: 90%;
  border-radius: 0.25rem;
`

const Code = styled.code`
  ${style};
`

export default Code
