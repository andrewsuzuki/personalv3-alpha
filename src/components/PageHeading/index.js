import styled from 'react-emotion'
import { css } from 'emotion'

import { media } from 'theme'

export const style = css`
  text-align: center;
  padding: 3rem 0 3.5rem;

  ${media.lessThan('medium')} {
    padding: 2rem 0 2.7rem;
  }
`

const PageHeading = styled.div`
  ${style};
`

export default PageHeading
