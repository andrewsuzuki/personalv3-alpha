import styled from 'react-emotion'

const Clear = styled.div`
  clear: both;
`

export default Clear
