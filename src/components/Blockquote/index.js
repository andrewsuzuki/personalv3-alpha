import styled from 'react-emotion'
import { css } from 'emotion'

import { media } from 'theme'

export const style = css`
  font-size: 1.5rem;

  line-height: 1.5;
  font-style: italic;
  font-weight: 300;

  margin-top: 1em;
  padding-left: 3em;

  ${media.size('large')} {
    font-size: 1.4rem;
    padding-left: 2.5em;
  }

  ${media.size('medium')} {
    font-size: 1.3rem;
    padding-left: 2em;
  }

  ${media.lessThan('medium')} {
    font-size: 1.2rem;
    padding-left: 1.5em;
  }
`

const Blockquote = styled.blockquote`
  ${style};
`

export default Blockquote
