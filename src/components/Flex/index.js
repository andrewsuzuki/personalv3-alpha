import styled from 'react-emotion'

const Flex = styled.div(
  ({
    basis = 'auto',
    direction = 'row',
    grow = 0,
    halign = 'flex-start',
    shrink = 1,
    valign = 'flex-start',
  }) => ({
    display: 'flex',
    flexDirection: direction,
    flexGrow: grow,
    flexShrink: shrink,
    flexBasis: basis,
    justifyContent: direction === 'row' ? halign : valign,
    alignItems: direction === 'row' ? valign : halign,
  }),
)

export default Flex
