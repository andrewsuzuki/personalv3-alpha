import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import styled from 'react-emotion'
import { css } from 'emotion'

import { colors, media } from 'theme'

import Pill from 'components/Pill'

const Container = styled.div`
  display: flex;
  flex-direction: row;

  ${({ hideBelowLarge }) =>
    (hideBelowLarge
      ? css`
          ${media.lessThan('large')} {
            display: none;
          }
        `
      : css`
          margin-top: 15px;
          margin-bottom: 10px;
        `)};
`

const activePillLinkStyle = css`
  background-color: ${colors.pillLinkBackgroundActive} !important;
  color: ${colors.white} !important;
`

const items = [
  { category: 'programming', title: 'Programming' },
  { category: 'cycling', title: 'Cycling' },
  { category: 'music', title: 'Music' },
]

const QuickPills = ({ currentPostCategory, onClick, hideBelowLarge }) => (
  <Container hideBelowLarge={hideBelowLarge}>
    {items.map(({ category, title }) => (
      <Pill
        key={category}
        to={`/category/${category}`}
        activeClassName={activePillLinkStyle}
        isActive={(match) => match || currentPostCategory === category}
        onClick={onClick}
        title={`Category: ${category}`}
      >
        {title}
      </Pill>
    ))}
  </Container>
)

QuickPills.propTypes = {
  currentPostCategory: PropTypes.string,
  hideBelowLarge: PropTypes.bool,
  onClick: PropTypes.func,
}

QuickPills.defaultProps = {
  currentPostCategory: null,
  hideBelowLarge: true,
  onClick: null,
}

export default connect(
  (state) => ({ currentPostCategory: state.currentPostCategory }),
  null,
  null,
  { pure: false }, // not pure because isActive depends on react-router location context
)(QuickPills)
