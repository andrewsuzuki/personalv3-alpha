import React from 'react'
import styled from 'react-emotion'

import { colors } from 'theme'

import Link from 'components/Link'
import H1 from 'components/H1'
import WrapperFull from 'components/WrapperFull'

import QuickPills from './QuickPills'
import Nav from './Nav'

import { responsiveHeaderHeight } from './common'

const Container = styled.header`
  margin-bottom: 15px;
`

const InnerWrapper = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: space-between;

  ${responsiveHeaderHeight};
`

const LogoLink = styled(Link)`
  color: ${colors.text};
`

const Logo = styled(H1)`
  font-size: 1.2rem;
  white-space: nowrap;
`

const Header = () => (
  <Container>
    <WrapperFull>
      <InnerWrapper>
        <LogoLink to="/" title="Andrew Suzuki">
          <Logo>Andrew Suzuki</Logo>
        </LogoLink>
        <QuickPills />
        <Nav />
      </InnerWrapper>
    </WrapperFull>
  </Container>
)

Header.propTypes = {}

export default Header
