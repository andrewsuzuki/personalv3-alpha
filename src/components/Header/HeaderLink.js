import React from 'react'
import styled from 'react-emotion'
import PropTypes from 'prop-types'
import { css } from 'emotion'

import { media, colors } from 'theme'

import Link from 'components/Link'

import { responsiveHeaderHeight } from './common'

const HeaderLinkNoActive = styled(Link)`
  display: flex;
  flex-direction: row;
  align-items: center;
  position: relative;
  padding: 0 15px;
  color: ${colors.text};

  ${responsiveHeaderHeight};
`

HeaderLinkNoActive.propTypes = {
  children: PropTypes.node.isRequired,
}

const activeStyle = css`
  color: ${colors.link} !important;

  &::after {
    ${media.greaterThan('small')} {
      content: '';
      position: absolute;
      top: 0;
      width: 100%;
      height: 1px;
      background: ${colors.link};
      left: 0;
      right: 0;
      z-index: 1;
    }
  }

  &:hover {
    color: ${colors.linkHover} !important;

    &::after {
      background: ${colors.linkHover};
    }
  }
`

const HeaderLink = (props) => <HeaderLinkNoActive activeClassName={activeStyle} {...props} />

export default HeaderLink
