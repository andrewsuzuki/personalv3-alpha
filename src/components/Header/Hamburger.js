import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'
import { css } from 'emotion'

import { colors } from 'theme'

const Outer = styled.button`
  outline: 0;
  font: inherit;
  overflow: visible;
  margin: 0 -5px 0 0;
  padding: 5px 5px 3px;
  cursor: pointer;
  transition-timing-function: linear;
  transition-duration: 0.15s;
  transition-property: opacity, filter;
  text-transform: none;
  color: inherit;
  border: 0;
  background-color: transparent;
`

const Box = styled.span`
  perspective: 40px;
  position: relative;
  display: inline-block;
  width: 30px;
  height: 18px;
`

const Inner = styled.span`
  background-color: ${colors.text};

  ${(props) =>
    props.active &&
    css`
      transform: rotateX(180deg);
      background-color: transparent;
    `};

  top: 50%;

  &,
  &::before,
  &::after {
    width: 30px;
    height: 3px;

    transition: transform 0.15s cubic-bezier(0.645, 0.045, 0.355, 1),
      background-color 0s 0.1s cubic-bezier(0.645, 0.045, 0.355, 1);
    display: block;
    position: absolute;
    border-radius: 3px;
  }

  &::before,
  &::after {
    background-color: ${colors.text};
    content: '';
  }

  &::before {
    top: -8px;

    ${(props) =>
    props.active &&
      css`
        transform: translate3d(0, 8px, 0) rotate(45deg);
      `};
  }

  &::after {
    bottom: -8px;

    ${(props) =>
    props.active &&
      css`
        transform: translate3d(0, -8px, 0) rotate(-45deg);
      `};
  }
`

const Hamburger = ({ onClick, active }) => (
  <Outer onClick={onClick}>
    <Box>
      <Inner active={active} />
    </Box>
  </Outer>
)

Hamburger.propTypes = {
  onClick: PropTypes.func.isRequired,
  active: PropTypes.bool.isRequired,
}

export default Hamburger
