import React from 'react'
import styled from 'react-emotion'
import startsWith from 'lodash/startsWith'

import { media, colors } from 'theme'
import config from 'config'

import HeaderLink from './HeaderLink'
import HeaderLinkMobile from './HeaderLinkMobile'
import Hamburger from './Hamburger'
import QuickPills from './QuickPills'

import { responsiveMobileTrayTop, responsiveMobileTrayHeight } from './common'

// Common

function postsLinkIsActive (match, location) {
  if (match) {
    // Already matched (begins with /posts)
    return true
  }

  // Check if location begins with /tags
  const prefixes = config.pathPrefixesForPostsRelated
  if (prefixes.some((p) => startsWith(location.pathname, p))) {
    return true
  }

  return false
}

const menu = [
  { to: '/', title: 'Home', exact: true },
  { to: '/posts', title: 'Posts', exact: false, isActive: postsLinkIsActive },
  { to: '/work', title: 'Work', exact: true },
  { to: '/about', title: 'About', exact: true },
  { to: '/contact', title: 'Contact', exact: true },
]

const makeNavItems = (component, onClick = null) =>
  menu.map((item) => React.createElement(component, { key: item.to, onClick, ...item }, item.title))

// Mobile

const MobileContainerOuter = styled.div`
  ${media.greaterThan('medium')} {
    display: none;
  }
`

const MobileTray = styled.div`
  position: absolute;
  left: 0;
  width: 100%;
  overflow-x: hidden;
  overflow-y: auto;

  display: flex;
  flex-direction: column;
  align-items: center;

  background: ${colors.white};

  border-top: 1px solid ${colors.divider};
  border-bottom: 3px solid ${colors.divider};

  padding: 15px 0;

  ${responsiveMobileTrayTop};
  ${responsiveMobileTrayHeight};
`

class MobileNav extends React.Component {
  constructor (props) {
    super(props)

    this.state = {
      show: false,
    }
  }

  toggle = () => {
    this.setState({
      show: !this.state.show,
    })
  }

  render () {
    const { show } = this.state

    return (
      <MobileContainerOuter>
        <Hamburger active={show} onClick={this.toggle} />
        {show && (
          <MobileTray>
            {makeNavItems(HeaderLinkMobile, this.toggle)}
            <QuickPills onClick={this.toggle} hideBelowLarge={false} />
          </MobileTray>
        )}
      </MobileContainerOuter>
    )
  }
}

// Normal

const NormalContainer = styled.nav`
  display: flex;
  flex-direction: row;
  align-items: stretch;
  overflow-x: auto;
  overflow-y: hidden;
  -webkit-overflow-scrolling: touch;
  height: 100%;

  ${media.lessThan('medium')} {
    display: none;
  }
`

const NormalNav = () => <NormalContainer>{makeNavItems(HeaderLink)}</NormalContainer>

// Everything

const Nav = () => (
  <div>
    <MobileNav />
    <NormalNav />
  </div>
)

export default Nav
