import { css } from 'emotion'
import { media } from 'theme'

// also used in ./HeaderLink
const responsiveHeaderHeight = css`
  height: 60px;
  ${media.lessThan('medium')} {
    height: 50px;
  }
`

const responsiveMobileTrayTop = css`
  top: 50px;
`

const responsiveMobileTrayHeight = css`
  max-height: calc(100vh - 50px);
`

export default {
  responsiveHeaderHeight,
  responsiveMobileTrayTop,
  responsiveMobileTrayHeight,
}
