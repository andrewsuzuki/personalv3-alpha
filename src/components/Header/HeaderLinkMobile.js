import React from 'react'
import styled from 'react-emotion'
import PropTypes from 'prop-types'
import { css } from 'emotion'

import { colors } from 'theme'

import Link from 'components/Link'

const HeaderLinkMobileNoActive = styled(Link)`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: center;
  position: relative;
  padding: 10px 15px;
  color: ${colors.text};
  width: 100%;
`

HeaderLinkMobileNoActive.propTypes = {
  children: PropTypes.node.isRequired,
}

const activeStyle = css`
  color: ${colors.link} !important;

  &:hover {
    color: ${colors.linkHover} !important;
  }
`

const HeaderLinkMobile = (props) => (
  <HeaderLinkMobileNoActive activeClassName={activeStyle} {...props} />
)

export default HeaderLinkMobile
