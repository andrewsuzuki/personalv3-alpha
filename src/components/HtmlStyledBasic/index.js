import styled from 'react-emotion'

import { style as pStyle } from 'components/Paragraph'
import { style as linkStyle } from 'components/Link'
import { style as hrStyle } from 'components/HorizontalRule'
import { style as preStyle } from 'components/Pre'
import { style as codeStyle } from 'components/Code'
import { style as ulStyle } from 'components/Ul'
import { style as olStyle } from 'components/Ol'
import { style as liStyle } from 'components/Li'
import { style as blockquoteStyle } from 'components/Blockquote'
import { style as tableStyle } from 'components/Table'
import { style as h2Style } from 'components/H2'
import { style as h3Style } from 'components/H3'
import { style as h4Style } from 'components/H4'
import { style as h5Style } from 'components/H5'
import { style as h6Style } from 'components/H6'

const HtmlStyledBasic = styled.section`
  & > :first-child {
    margin-top: 0 !important;
    padding-top: 0 !important;
  }

  & p {
    ${pStyle};
  }
  & a {
    ${linkStyle};
  }
  & hr {
    ${hrStyle};
  }
  & pre {
    ${preStyle};
  }
  & :not(pre) > code {
    ${codeStyle};
  }
  & ul {
    ${ulStyle};
  }
  & ol {
    ${olStyle};
  }
  & li {
    ${liStyle};
  }
  & blockquote {
    ${blockquoteStyle};
  }
  & table {
    ${tableStyle};
  }

  & h2 {
    ${h2Style};
  }
  & h3 {
    ${h3Style};
  }
  & h4 {
    ${h4Style};
  }
  & h5 {
    ${h5Style};
  }
  & h6 {
    ${h6Style};
  }

  & img,
  & .gatsby-resp-image-link {
    max-width: 100%;
  }

  & .footnotes {
    font-size: 0.8rem;
    margin-top: 2.5rem;

    & ol li {
      margin-top: 0.7em;

      &:first-of-type {
        margin-top: 0;
      }

      & p {
        margin-top: 0.2em;
      }
    }
  }
`

export default HtmlStyledBasic
