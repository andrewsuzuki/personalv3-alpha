import styled from 'react-emotion'
import { css } from 'emotion'

import { media } from 'theme'

export const style = css`
  font-weight: 300;

  font-size: 1.5rem;
  ${media.between('small', 'large')} {
    font-size: 1.5rem;
  }
  ${media.lessThan('small')} {
    font-size: 1.5rem;
  }

  margin-top: 1em;

  &:first-child {
    margin-top: 0;
  }
`

const H5 = styled.h5`
  ${style};
`

export default H5
