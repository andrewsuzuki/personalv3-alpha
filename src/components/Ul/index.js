import styled from 'react-emotion'
import { css } from 'emotion'

export const style = css`
  list-style-position: inside;
  margin-top: 1em;

  & ul {
    margin-top: 0;
    margin-left: 1em;
  }
  & ul ul {
    margin-left: 2em;
  }
  & ul ul ul {
    margin-left: 3em;
  }
`

const Ul = styled.ul`
  ${style};
`

export default Ul
