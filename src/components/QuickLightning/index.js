import React from 'react'
import PropTypes from 'prop-types'
import styled from 'react-emotion'

const QuickLightningSpan = styled.span`
  display: inline-block;
  font-size: ${({ smaller }) => (smaller ? 0.7 : 1)}rem;
`

const QuickLightning = ({ smaller }) => (
  // NOTE: this satisfies jsx-a11y, but it doesn't recognize
  // that QuickLightningSpan is a span
  // eslint-disable-next-line jsx-a11y/accessible-emoji
  <QuickLightningSpan role="img" aria-label="Quick read" title="Quick read" smaller={smaller}>
    ⚡
  </QuickLightningSpan>
)

QuickLightning.propTypes = {
  smaller: PropTypes.bool,
}

QuickLightning.defaultProps = {
  smaller: false,
}

export default QuickLightning
