import styled from 'react-emotion'
import PropTypes from 'prop-types'

import { media } from 'theme'

const WrapperFull = styled.div`
  margin: 0 auto;
  padding: 0 20px;
  width: 100%;

  ${media.greaterThan('medium')} {
    width: 90%;
  }

  ${media.size('xxlarge')} {
    max-width: 1260px;
  }
`

WrapperFull.propTypes = {
  children: PropTypes.node.isRequired,
}

export default WrapperFull
