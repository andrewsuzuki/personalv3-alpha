import styled from 'react-emotion'
import { css } from 'emotion'

import { fonts, media } from 'theme'

export const style = css`
  text-align: center;

  font-family: ${fonts.headingSerif};
  font-weight: 300;
  line-height: 1.1;

  font-size: 3.2rem;
  ${media.between('medium', 'large')} {
    font-size: 2.8rem;
  }
  ${media.lessThan('medium')} {
    font-size: 2.5rem;
  }
`

const H2 = styled.h2`
  ${style};
`

export default H2
