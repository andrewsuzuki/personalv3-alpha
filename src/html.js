import React from 'react'
import PropTypes from 'prop-types'
import Typekit from 'react-typekit'

const Html = ({ headComponents, body, postBodyComponents }) => {
  /* eslint-disable react/no-danger */
  const gatsbyDiv = <div id="___gatsby" dangerouslySetInnerHTML={{ __html: body }} />
  /* eslint-enable react/no-danger */

  return (
    <html lang="en">
      <head>
        <meta name="referrer" content="origin" />
        <meta charSet="utf-8" />
        <meta name="description" content="Andrew Suzuki's website and blog" />
        <meta httpEquiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <title>Andrew Suzuki</title>
        <Typekit kitId="fwo4iiw" />
        {headComponents}
      </head>
      <body>
        {gatsbyDiv}
        {postBodyComponents}
      </body>
    </html>
  )
}

Html.propTypes = {
  headComponents: PropTypes.node.isRequired,
  body: PropTypes.node.isRequired,
  postBodyComponents: PropTypes.node.isRequired,
}

export default Html
