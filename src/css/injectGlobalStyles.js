/**
 * Inject global styles and fonts
 */

import { injectGlobal } from 'emotion'

import { fonts, media } from 'theme'

import injectPrismStyles from './prism'

export default function injectGlobalStyles () {
  /* eslint-disable no-unused-expressions */

  injectGlobal`
    * {
      margin: 0;
      padding: 0;
      box-sizing: border-box;
    }

    html {
      font-family: ${fonts.standard};
      font-size: 17px;
      line-height: 1.7;

      -webkit-font-smoothing: antialiased;
      -moz-osx-font-smoothing: grayscale;
    }

    ${media.lessThan('medium')} {
      /* Move spinner over a bit to make way for
         hamburger (default is right: 15px) */
      #nprogress .spinner {
        right: 70px !important;
      }
    }
  `

  injectPrismStyles()

  /* eslint-enable no-unused-expressions */
}
