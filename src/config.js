/**
 * Configuration and Constants
 */

const config = {
  // These location pathname prefixes will show
  // the Posts header link as active
  pathPrefixesForPostsRelated: ['/posts', '/tags', '/category'],

  // Send contact form submissions to this email (slightly obfuscated)
  contactFormToEmail: `${'andrew.b.suzuki'}@${'gmail.com'}`,
}

export default config
